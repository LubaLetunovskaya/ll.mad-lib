
// Luba and Kevin
// Lab Assignment 3: Mad Lib

#include <iostream>
#include <string>
#include <conio.h>
#include <vector>
#include <fstream>

using namespace std;

void MadLib(string* values, ostream& os = cout)
{
	os << " One day my " << values[0] << " friend and I decided to go to the " << values[1] << " game in " << values[2] 
	   <<  " We really wanted to see " << values[3] << " play"
	   << " So we " << values[4] << " in the " << values[5] << " and headed down to the " << values[6] << " and bought some " << values[7] 
	   << " We watched the game and it was " << values[8] << " We ate some " << values[9] << " and drank some " << values[10] 
	   << " We had a " << values[11] << " time, and can't wait to go again.";
}

int main()
{
	char input;
	ofstream ofs("madlib.txt");

	const int NUM_VALUES = 12;
	string values[NUM_VALUES];
	values[0] = "Enter an adjective: ";
	values[1] = "Enter a sport: ";
	values[2] = "Enter a city: ";
	values[3] = "Enter a person: ";
	values[4] = "Enter an action verb: ";
	values[5] = "Enter a vehicle: ";
	values[6] = "Enter a place: ";
	values[7] = "Enter a noun(thing, plural): ";
	values[8] = "Enter an adjective (describing word): ";
	values[9] = "Enter a food (plural): ";
	values[10] = "Enter a liquid: ";
	values[11] = "Enter an adjective (describing wordd): ";

	// must use a loop to collect the values
	for (int i = 0; i < NUM_VALUES; i++)
	{
		cout << values[i];
		cin >> values[i];
		cout << "\n";
	}

	// save file
	MadLib(values);
	cout << "\n";
	cout << "Would you like to save this story? (enter y/n)";
    cin >> input;

	if (input == 'y'|| input =='Y')
	{
		MadLib(values, ofs);
	}
	

	(void)_getch;
	return 0;
}